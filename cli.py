"""
Rubik's Cube Solver

Usage:
  cli.py calibrate
  cli.py string <pieces>...
  cli.py video
  cli.py -h | --help
"""

import os
from docopt import docopt
from src import main
from src import solve
from src import read


def run(argv=None):
    # Change working directory to the data directory directory for easy loading of tables
    data_path = os.path.dirname(os.path.realpath(__file__)) + '/data'
    if not os.path.exists(data_path):
        os.mkdir(data_path)
    os.chdir(data_path)

    args = docopt(__doc__, argv=argv)

    if args['calibrate']:
        read.calibrate()

    elif args['string']:
        cube_str = ' '.join(args['<pieces>'])
        solve.solve_str(cube_str)

    elif args['video']:
        main.main()


if __name__ == '__main__':
    run()
