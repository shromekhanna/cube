from .solve import solve_str
from .read import video_main


def to_string(cube_matrix):
    color_to_face = {}
    for face in 'UDLRFB':
        color_to_face[cube_matrix[face][1][1]] = face

    for face in cube_matrix.keys():
        for row in range(len(cube_matrix[face])):
            for i in range(len(cube_matrix[face][row])):
                cube_matrix[face][row][i] = color_to_face[cube_matrix[face][row][i]]

    print(cube_matrix)
    conversion_matrix = {'R': [[26, 3, 28], [17, 'R', 21], [37, 11, 47]],
                         'L': [[32, 7, 34], [23, 'L', 19], [43, 15, 41]],
                         'U': [[30, 4, 27], [6, 'U', 2], [33, 0, 24]],
                         'D': [[39, 8, 36], [14, 'D', 10], [42, 12, 45]],
                         'F': [[35, 1, 25], [18, 'F', 16], [40, 9, 38]],
                         'B': [[29, 5, 31], [20, 'B', 22], [46, 13, 44]]}
    ans = [' '] * 48
    for face in 'UDLRFB':
        for i in range(3):
            for j in range(3):
                if i == 1 and j == 1:
                    continue
                ans[conversion_matrix[face][i][j]] = cube_matrix[face][i][j]

    edges = [ans[2*i:2*(i+1)] for i in range(12)]
    corners = [ans[3*i:3*(i+1)] for i in range(8, 16)]
    ans = edges + corners
    for i in range(len(ans)):
        ans[i] = ''.join(ans[i])

    return ' '.join(ans)


def main():
    cube_matrix = video_main()
    cube_str = to_string(cube_matrix)
    print(cube_str)
    solve_str(cube_str)


if __name__ == '__main__':
    main()
