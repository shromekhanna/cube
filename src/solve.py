import os
import copy
import pickle
from .cube import Cube
from collections import deque


def thistlethwaite(cube_c):
    final_ans = ''
    ans = ''

    # Maximum level to search to for each thistletwaite phase
    level_list = [7, 10, 13, 15]

    for phase in range(4):
        prune_file = 'prune' + str(phase) + '.dat'

        if os.path.isfile(prune_file):
            with open(prune_file, 'rb') as f:
                prune_table = pickle.load(f)
        else:
            print('Prune table not found. Generating a new one. This may take a while.')
            prune_table = prune(phase)
            with open(prune_file, 'wb') as f:
                pickle.dump(prune_table, f)

        is_solved = False

        # Iterative Depth First Search to find solution at nearest level
        for level_max in range(level_list[phase]):
            # Deepcopy (overridden) since Cube.state is mutable
            cube_tmp = copy.deepcopy(cube_c)

            is_solved, cube_tmp, ans = tree_search(cube_tmp, phase, level_max, prune_table)
            if is_solved:
                cube_c = cube_tmp
                break

        assert is_solved
        final_ans += ' ' + ans.strip()

    final_ans = final_ans.replace('  ', ' ').strip()
    print("Solution:", final_ans)
    print("Number of Moves:", final_ans.count(' ')+1)


def tree_search(cube_c, phase, level, prune_table, prev_move='X'):
    # Returns 3 values: bool(is cube solved),
    # state of cube after solution and set of moves applied

    # Return true if already solved for the phase
    if cube_c.is_phase(phase):
        return True, cube_c, ''

    # Return false if current state is guaranteed to
    # take more moves according to the prune table
    cid = cube_c.id(phase)
    if cid in prune_table.keys() and prune_table[cid]-1 > level:
        return False, None, ''

    # Return false when maximum level is reached
    if level <= 0:
        return False, None, ''

    moves = (' '.join(cube_c.move_set[phase:])).split(' ')
    for move in moves:
        # Rotating opposite faces in either order has the same result, eg R L = L R
        # We only need to check for one case
        # We also shouldn't repeat the previous move as U U = U2
        if((move[0] == prev_move[0]) or
                (move[0] == 'R' and prev_move[0] == 'L') or
                (move[0] == 'D' and prev_move[0] == 'U') or
                (move[0] == 'B' and prev_move[0] == 'F')):
            continue

        cube_tmp = copy.deepcopy(cube_c)
        cube_tmp.rotate(move)

        is_solved, cube_tmp, ans = tree_search(cube_tmp, phase, level - 1, prune_table, move)
        if is_solved:
            return True, cube_tmp, (move + ' ' + ans).strip()

    return False, None, ''


def prune(phase):
    # Generate prune tables for a particular phase
    # Start from the solved state and make maximum number of permissible moves
    cube_c = Cube()
    moves = (' '.join(cube_c.move_set[phase:])).split(' ')
    prune_table = {cube_c.id(phase): 1}

    # Push solved state into BFS queue
    # [Element, Level]
    queue = deque([[cube_c, 1]])
    while queue:
        curr = queue.popleft()

        # Max depth to calculate prune table to
        if curr[1] > [7, 10, 13, 15][phase]:
            continue

        for m in moves:
            tmp = copy.deepcopy(curr[0])
            tmp.rotate(m)
            tmp_id = tmp.id(phase)

            # Visit state if it hasn't been visited before
            if tmp_id not in prune_table.keys():
                queue.append([tmp, curr[1]+1])
                prune_table[tmp_id] = curr[1]+1

    return prune_table


def solve_str(cube_str):
    cube_s = Cube(cube_str)
    thistlethwaite(cube_s)
