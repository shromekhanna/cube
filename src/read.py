import cv2
import numpy as np
import pickle
import os

# Lists in the format [B, G, R]
color_map_default = {'red': [0, 0, 255],
                     'green': [0, 255, 0],
                     'blue': [255, 0, 0],
                     'orange': [0, 165, 255],
                     'yellow': [0, 255, 255],
                     'white': [255, 255, 255]}


def avg_color(img):
    average_color_per_row = np.average(img, axis=0)
    average_color = np.average(average_color_per_row, axis=0)
    return average_color


def identify_color(c, color_map):
    diff_min = np.inf
    color = ''

    for i in color_map:
        diff = sum([abs(a - b) for a, b in zip(c, color_map[i])])
        if diff < diff_min:
            diff_min = diff
            color = i

    return color


def calibrate():
    colors = {}
    cap = cv2.VideoCapture(0)
    order = ['green', 'orange', 'blue', 'red', 'white', 'yellow']
    txt = 'Align face in Squares. Press Enter to capture.'
    for face in order:
        while True:
            ret, frame = cap.read()
            frame_display = frame.copy()                                # Separate the display and processing Frames

            p = int(min(frame.shape[0], frame.shape[1]) / 50)           # size of cubie regions
            r = int(min(frame.shape[0], frame.shape[1]) / 10)           # distance b/w points
            x, y = int(frame.shape[1] / 2), int(frame.shape[0] / 2)     # Center

            # Define and mark capture regions for the face
            current_color = [0, 0, 0]
            for yi in [y - r, y, y + r]:
                for xi in [x - r, x, x + r]:
                    frame_display = cv2.rectangle(frame_display, (xi - p, yi - p), (xi + p, yi + p), (255, 0, 0), 2)
                    block = frame[yi - p:yi + p, xi - p:xi + p]
                    current_color += avg_color(block)

            current_color = list(map(lambda i: i/9, current_color))
            colors[face] = current_color

            # Flip and display image
            frame_display = cv2.flip(frame_display, 1)
            # Add text prompt at the bottom left
            frame_display = cv2.putText(frame_display, txt, (0, int(frame.shape[0]) - 5),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))

            cv2.imshow('Frame', frame_display)

            # Identify Enter Key
            if cv2.waitKey(1) == 13:
                break

        txt = face + ' has been captured.'

    cap.release()
    cv2.destroyAllWindows()

    with open('colors.dat', 'wb') as color_file:
        pickle.dump(colors, color_file)


def video_main():
    cube = {}
    cap = cv2.VideoCapture(0)
    order = 'FLBRUD'
    txt = 'Align face in Squares. Press Enter to capture.'

    if os.path.isfile('colors.dat'):
        with open('colors.dat', 'rb') as color_file:
            color_map = pickle.load(color_file)
    else:
        color_map = color_map_default

    for face in order:
        while True:
            ret, frame = cap.read()
            frame_display = frame.copy()                                # Separate the display and processing Frames

            p = int(min(frame.shape[0], frame.shape[1]) / 50)           # size of cubie regions
            r = int(min(frame.shape[0], frame.shape[1]) / 10)           # distance b/w points
            x, y = int(frame.shape[1]/2), int(frame.shape[0]/2)         # Center

            # Define and mark capture regions for the face
            res = []
            for yi in [y-r, y, y+r]:
                for xi in [x-r, x, x+r]:
                    frame_display = cv2.rectangle(frame_display, (xi-p, yi-p), (xi+p, yi+p), (255, 0, 0), 2)
                    block = frame[yi-p:yi+p, xi-p:xi+p]
                    res.append(identify_color(avg_color(block), color_map))

            # Convert result to 3x3 matrix
            res = [res[0:3], res[3:6], res[6:9]]

            # Display Captured Faces
            # Use default colors for display since they're more 'accurate'
            face_size = 20
            for i in range(3):
                for j in range(3):
                    frame_display = cv2.rectangle(frame_display,
                                                  (face_size*(j+1), face_size*(i+1)),
                                                  (face_size*(j+2), face_size*(i+2)),
                                                  color_map_default[res[i][j]], -1)

            # Flip and display image
            frame_display = cv2.flip(frame_display, 1)
            # Add text prompt at the bottom left
            frame_display = cv2.putText(frame_display, txt, (0, int(frame.shape[0])-5),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))

            cv2.imshow('Frame', frame_display)

            # Identify Enter Key
            if cv2.waitKey(1) == 13:
                break

        cv2.imwrite('cam.jpg', frame)
        cube[face] = res
        txt = face + ' has been captured.'

    cap.release()
    cv2.destroyAllWindows()

    return cube


if __name__ == '__main__':
    video_main()
