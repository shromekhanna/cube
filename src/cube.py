class Cube:
    solved_state_str = 'UF UR UB UL DF DR DB DL FR FL BR BL UFR URB UBL ULF DRF DFL DLB DBR'

    # Move sets according to the phases they're eliminated
    move_set = ["F B F' B'",
                "L R L' R'",
                "U D U' D'",
                "U2 D2 L2 R2 F2 B2"]

    rotations = {'U': [[0, 3, 2, 1], [12, 15, 14, 13]],
                 'D': [[4, 5, 6, 7], [16, 19, 18, 17]],
                 'L': [[3, 9, 7, 11], [14, 15, 17, 18]],
                 'R': [[1, 10, 5, 8], [12, 13, 19, 16]],
                 'F': [[0, 8, 4, 9], [15, 12, 16, 17]],
                 'B': [[2, 11, 6, 10], [13, 14, 18, 19]]}

    def __init__(self, state_initial=None):
        if state_initial is None:
            # The position of the cube is stored in a list of length 40.
            # The first 20 elements hold the permutation. (12 edges, 8 corners)
            # The next 20 hold the orientation. (12 edges, 8 corners)
            # The edge orientation is 0 or 1, and corner orientation is 0, 1 or 2
            # The solved (default) state is given as:
            self.state = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            return

        state_initial = state_initial.split(' ')
        state_solved = self.solved_state_str.split(' ')

        state_solved_sorted = [''.join(sorted(i)) for i in state_solved]
        state_sorted = [''.join(sorted(i)) for i in state_initial]

        permutation = [state_solved_sorted.index(i) for i in state_sorted]
        orientation = [0 if state_solved[permutation[i]] == state_initial[i] else 1 for i in range(12)]

        for corner in state_initial[12:]:
            for i in range(3):
                if corner[i] == 'U' or corner[i] == 'D':
                    orientation.append(i)

        self.state = permutation + orientation

    def __deepcopy__(self, cube_tmp):
        # Override deepcopy. This *really* speeds things up.
        cube_tmp = Cube()
        cube_tmp.state = self.state.copy()
        return cube_tmp

    def rotate(self, instructions):
        for instruction in instructions.split(' '):
            face = instruction[0]
            turns = 1
            if instruction[1:] == '2':
                turns = 2
            elif instruction[1:] == '3' or instruction[1:] == '\'' or instruction[1:] == '-':
                turns = 3

            # Swaps for each rotation.
            # Much faster than performing rotation multiple times in a loop
            # Since this function is called a huge number of times, this speed adds up
            if turns == 1:
                for i in self.rotations[instruction[0]]:
                    self.state[i[0]], self.state[i[1]], self.state[i[2]], self.state[i[3]] = \
                        self.state[i[3]], self.state[i[0]], self.state[i[1]], self.state[i[2]]

                    self.state[20 + i[0]], self.state[20 + i[1]], self.state[20 + i[2]], self.state[20 + i[3]] = \
                        self.state[20 + i[3]], self.state[20 + i[0]], self.state[20 + i[1]], self.state[20 + i[2]]
            elif turns == 2:
                for i in self.rotations[instruction[0]]:
                    self.state[i[0]], self.state[i[1]], self.state[i[2]], self.state[i[3]] = \
                        self.state[i[2]], self.state[i[3]], self.state[i[0]], self.state[i[1]]

                    self.state[20 + i[0]], self.state[20 + i[1]], self.state[20 + i[2]], self.state[20 + i[3]] = \
                        self.state[20 + i[2]], self.state[20 + i[3]], self.state[20 + i[0]], self.state[20 + i[1]]
            elif turns == 3:
                for i in self.rotations[instruction[0]]:
                    self.state[i[0]], self.state[i[1]], self.state[i[2]], self.state[i[3]] = \
                        self.state[i[1]], self.state[i[2]], self.state[i[3]], self.state[i[0]]

                    self.state[20 + i[0]], self.state[20 + i[1]], self.state[20 + i[2]], self.state[20 + i[3]] = \
                        self.state[20 + i[1]], self.state[20 + i[2]], self.state[20 + i[3]], self.state[20 + i[0]]

            # Double turns don't affect orientation
            if turns != 2:
                # Edge orientations
                if face in 'FB':
                    for i in self.rotations[face][0]:
                        self.state[20 + i] ^= 1

                # Corner orientations
                if face in 'FBLR':
                    for i in range(4):
                        self.state[20 + self.rotations[face][1][i]] = \
                            (self.state[20 + self.rotations[face][1][i]] + [1, 2, 1, 2][i]) % 3

    def is_phase(self, phase):
        permutation = self.state[:20]
        orientation = self.state[20:]
        if phase == 0:
            # Edge orientation
            if any(orientation[:12]):
                return False

        elif phase == 1:
            # Corner orientation
            if any(orientation[12:]):
                return False
            # Middle Edges in the middle
            if compare(permutation, [8, 9, 10, 11]):
                return False

        elif phase == 2:
            # Edges in Slices
            if (compare(permutation, [0, 2, 4, 6]) or
                    compare(permutation, [1, 3, 5, 7]) or
                    compare(permutation, [8, 9, 10, 11])):
                return False
            # Corners in orbits
            if compare(permutation, [12, 14, 17, 19]) or compare(permutation, [13, 15, 16, 18]):
                return False
            # Edge-Corner Parity (odd/even inversions)
            parity = True
            for i in range(12, 20):
                for j in range(i + 1, 20):
                    parity ^= permutation[i] > permutation[j]
            return parity

        elif phase == 3:
            # Completely solved
            for i in range(20):
                if permutation[i] != i or orientation[i] != 0:
                    return False

        return True

    def to_state(self):
        permutation = self.state[:20]
        orientation = self.state[20:]
        temp = self.solved_state_str.split(' ')
        ans = [temp[i] for i in permutation]

        for i in range(20):
            ans[i] = rotate_string(ans[i], orientation[i])

        return ' '.join(ans)

    def id(self, phase):
        # Computes the id of a state for the given phase
        # Used as index in the prune tables
        if phase == 0:
            # Edge orientation
            ans = 0
            for i in range(20, 32):
                ans <<= 1
                ans += self.state[i]

            return ans

        elif phase == 1:
            # Middle edges in the middle
            ans = [0, 0]
            for e in range(12):
                ans[0] |= int(self.state[e] / 8) << e

            # Corner orientation
            for i in range(32, 40):
                ans[1] *= 3
                ans[1] += self.state[i]

            return tuple(ans)

        elif phase == 2:
            ans = [0]*3

            # Each face only has same/opposite faces
            for e in range(12):
                ans[0] |= (2 if (self.state[e] > 7) else (self.state[e] & 1)) << (2 * e)
            for c in range(8):
                ans[1] |= ((self.state[c + 12] - 12) & 5) << (3 * c)

            # Edge-Corner Parity (odd/even inversions)
            for i in range(12, 20):
                for j in range(i+1, 20):
                    ans[2] ^= self.state[i] > self.state[j]

            return tuple(ans)

        else:
            # Fully solved
            ans = [0, 0]
            n = 20
            # Index permutation only till n-2, since parity must be even
            for i in range(0, n-2):
                ans[0] = ans[0] * (n-i+1)
                for j in range(i+1, n):
                    if self.state[i] > self.state[j]:
                        ans[0] += 1

            # Index orientation
            for i in range(n):
                ans[1] *= 3
                ans[1] += self.state[20+i]

            return tuple(ans)


# Tiny helpers
def compare(permutation, l):
    p = [permutation[i] for i in l]
    if sorted(p) != l:
        return True
    return False


def rotate_string(string, n):
    return string[-n:] + string[:-n]
