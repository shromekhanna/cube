# Rubik's Cuble Solver #
This package contains an implementation of Thistlethwaite's four-phase
algorithm for solving a Rubik's Cube. This can solve any cube state in
a maximum of 45 moves. In each phase of this algorithm, the number of 
possible moves is reduced. The phases are as follows:

1. **<U, D, L, R, F, B> to <U, D, L, R, F2, B2>**
    
    Edges should be oriented correctly
    
2. **<U, D, L, R, F2, B2> to <U, D, L2, R2, F2, B2>**
 
    Corners should be oriented correctly and middle edges (`FR`, `FL`, `BR`, `BL`) should be in the middle layer
    
3. **<U, D, L2, R2, F2, B2> to <U2, D2, L2, R2, F2, B2>**
   
    A face should only have its own stickers, or those of the face directly opposite to it. Edge-corner parity should be verified. 
   
4. **<U2, D2, L2, R2, F2, B2> to {I}**

    The cube is completely solved.
    
The cube state is represented as a list of 40 elements. The first 20 contain
 the permutation and the next 20 contain the orientation, 12 for edges, 8 
 for corners in each case.
## Usage ##
 The tool accepts two forms of input, a string containing the cube state
 and video input from the computer's camera.
 
 For video input, first calibrate the colors using
 
` python cli.py calibrate`

This is done using a solved cube. Faces are shown in the order:
Green-Orange-Blue-Red-White-Yellow (FLBRUD). The same order is used for the
video inupt, which is run using

`python cli.py video`

To run the tool with a string as the input, execute

`python cli.py string UF UR UB UL DF DR DB DL FR FL BR BL UFR URB UBL ULF DRF DFL DLB DBR`

The string input format is given by:
 
 `UF UR UB UL DF DR DB DL FR FL BR BL UFR URB UBL ULF DRF DFL DLB DBR`
 
This string represents the solved cube. The image should make this clearer.

![string_notation.png](https://bitbucket.org/repo/Ggn7R5d/images/1835693502-string_notation.png)

## Performance ##
On [Tomas Rokicki's Data Set](http://tomas.rokicki.com/cubecontest/testdata.txt), the tool ran with an average of ~16 moves, taking about 600ms per solution.